import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {UserComponent} from "./components/user/user.component";
import {HelloComponent} from "./components/hello/hello.compenent";
import {TravelComponent} from "./components/travel/travel";
import { CodeboundComponent } from './components/codebound/codebound.component';
import {MathComponent} from "./components/math/math.component";


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HelloComponent,
    TravelComponent,
    CodeboundComponent,
    MathComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

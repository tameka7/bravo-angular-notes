import { Component } from "@angular/core";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls:['./user.component.css']
})

export class UserComponent {
///PROPERTIES - like an attribute of the Component
//   firstName = 'Clark';
//   lastName = 'Kent';
//   age = 65;
  firstName: string
  lastName: string
  age: number;


///Methods
  constructor() {
  console.log('hello from user component')

    this.firstName ='clark';
    this.lastName = 'kent';
    this.greeting();

  console.log(this.age);//65
  this.hasBirthday()
    console.log(this.age)//66
  }
  greeting (){
    console.log('Hello there' + this.firstName);
  }
  hasBirthday(){
    this.age +=1;
  }

}///end of class dont delete
